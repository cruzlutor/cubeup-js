'use strict';

//let Phaser = require('./lib/phaser')
let BootState = require('./states/boot/BootState'),
    GameState = require('./states/game/GameState');

class Game extends Phaser.Game {
    constructor() {

        /* draw a canvas */
        super(320, 533, Phaser.AUTO, null);

        /* add game states */
        this.state.add('boot', BootState, false);
        this.state.add('game', GameState, false);
        
        /* init boot state */
        this.state.start('game');
    }
}

new Game();
