'use strict';

/**
 * BootState
 */
class BootState extends Phaser.State {

    preload(){
				console.log('preloaded!');
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
				this.scale.forceLandscape = true;
				this.scale.pageAlignHorizontally = true;
				this.scale.setScreenSize(true);
    }

    create(){
        console.log('created!');
    }
}

module.exports = BootState;
