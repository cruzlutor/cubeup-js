'use strict';

class GameState extends Phaser.State {

    preload(){
        console.log('preloaded!');
    }

    create(){
        this.stage.backgroundColor = 0xffffff;
        console.log('created!');
    }
}

module.exports = GameState;
