'use strict';

let BootState = require('./states/BootState'),
    MenuState = require('./states/MenuState'),
    GameState = require('./states/GameState');

class Game extends Phaser.Game {
    constructor() {

        /* draw a canvas */
        super(320, 533, Phaser.AUTO, null);

        /* add game states */
        this.state.add('boot', BootState, false);
        this.state.add('menu', MenuState, false);
        this.state.add('game', GameState, false);
        //this.stage.smoothed = false;

        /* init boot state */
        this.state.start('boot');
    }
}

new Game();
