(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

let BootState = require('./states/BootState'),
    MenuState = require('./states/MenuState'),
    GameState = require('./states/GameState');

class Game extends Phaser.Game {
    constructor() {

        /* draw a canvas */
        super(320, 533, Phaser.AUTO, null);

        /* add game states */
        this.state.add('boot', BootState, false);
        this.state.add('menu', MenuState, false);
        this.state.add('game', GameState, false);
        //this.stage.smoothed = false;

        /* init boot state */
        this.state.start('boot');
    }
}

new Game();

},{"./states/BootState":2,"./states/GameState":3,"./states/MenuState":4}],2:[function(require,module,exports){
'use strict';

/**
 * BootState
 */
class BootState extends Phaser.State {

    preload(){
        console.log('boot preloader!');
        this.stage.backgroundColor = 0x999999;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //this.scale.forceLandscape = true;
        //this.scale.pageAlignHorizontally = true;
    }

    create(){
        this.game.input.onTap.add(this.goMenu, this);
    }

    goMenu(){
        this.state.start('menu');
    }
}

module.exports = BootState;

},{}],3:[function(require,module,exports){
'use strict';

/**
 * GameState
 */
class GameState extends Phaser.State {

    preload(){
        console.log('preloaded!');
    }

    create(){
        this.stage.backgroundColor = 0xffffff;
        console.log('created!');
    }
}

module.exports = GameState;

},{}],4:[function(require,module,exports){
'use strict';

/**
 * MenuState
 */
class MenuState extends Phaser.State {

    preload(){
        this.GUI = {};
        this.stage.backgroundColor = 0xffffff;

        // load images resources for the menu
        this.game.load.image('logo', './../img/logo.png');
    }

    create(){
        let button = this.game.add.button(this.game.world.centerX, this.game.world.centerY, null, this.goPlay, this);
        let label = this.game.add.text(0, 0, "Play", {'font': '10px Arial','fill': 'white'});
        let bmd = this.game.add.bitmapData(100, 30);

        // create bitmapdata
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, 100, 30);
        bmd.ctx.fillStyle = '#4CA5FF';
        bmd.ctx.fill();


        let background = this.game.add.sprite(0, 0, bmd);

        // center elements
        background.anchor.setTo(0.5, 0.5);
        label.anchor.set(0.5, 0.5);
        button.anchor.setTo(0.5, 0.5);

        // add elements to button
        button.addChild(background);
        button.addChild(label);

        this.GUI['playButn'] = button;


        // draw images
        let logo = this.game.add.sprite(this.game.world.centerX, 50, 'logo');
        logo.anchor.x = 0.5;
        logo.scale.setTo(0.25,0.25);

   }

    goPlay(){
        this.state.start('game');
    }

    update(){

    }
}   


module.exports = MenuState;
},{}]},{},[1]);
