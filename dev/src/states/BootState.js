'use strict';

/**
 * BootState
 */
class BootState extends Phaser.State {

    preload(){
        console.log('boot preloader!');
        this.stage.backgroundColor = 0x999999;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        //this.scale.forceLandscape = true;
        //this.scale.pageAlignHorizontally = true;
    }

    create(){
        this.game.input.onTap.add(this.goMenu, this);
    }

    goMenu(){
        this.state.start('menu');
    }
}

module.exports = BootState;
