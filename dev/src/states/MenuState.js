'use strict';

/**
 * MenuState
 */
class MenuState extends Phaser.State {

    preload(){
        this.GUI = {};
        this.stage.backgroundColor = 0xffffff;

        // load images resources for the menu
        this.game.load.image('logo', './../img/logo.png');
    }

    create(){
        let button = this.game.add.button(this.game.world.centerX, this.game.world.centerY, null, this.goPlay, this);
        let label = this.game.add.text(0, 0, "Play", {'font': '10px Arial','fill': 'white'});
        let bmd = this.game.add.bitmapData(100, 30);

        // create bitmapdata
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, 100, 30);
        bmd.ctx.fillStyle = '#4CA5FF';
        bmd.ctx.fill();


        let background = this.game.add.sprite(0, 0, bmd);

        // center elements
        background.anchor.setTo(0.5, 0.5);
        label.anchor.set(0.5, 0.5);
        button.anchor.setTo(0.5, 0.5);

        // add elements to button
        button.addChild(background);
        button.addChild(label);

        this.GUI['playButn'] = button;


        // draw images
        let logo = this.game.add.sprite(this.game.world.centerX, 50, 'logo');
        logo.anchor.x = 0.5;
        logo.scale.setTo(0.25,0.25);

   }

    goPlay(){
        this.state.start('game');
    }

    update(){

    }
}   


module.exports = MenuState;