var gulp 	     = require('gulp'),
    connect      = require('gulp-connect'),
    watch 	     = require('gulp-watch'),
    browserify   = require('browserify'),
    babelify     = require('babelify'),
    buffer       = require('vinyl-buffer'),
    source       = require('vinyl-source-stream');


gulp.task('connect', function() {
    connect.server({
        root: '../dev/',
        livereload: true
    });
});


gulp.task('src', function() {
    return browserify({
        entries: ['../dev/src/app.js'],
        transform: [babelify.configure({
            only: '../dev/src/'
        })]
    })
    .bundle()
    .pipe(source('../dev/src/game.js'))
    .pipe(buffer())
    .pipe(gulp.dest('../dev/'))
    .pipe(connect.reload());
});


gulp.task('html', function(){
    return gulp.src('../dev/index.html')
        .pipe(connect.reload());
});


gulp.task('watch', function(){
    gulp.watch('../dev/src/**/*.js', ['src']);
    gulp.watch('../dev/index.html', ['html']);
})

gulp.task('default', ['connect', 'watch']);
